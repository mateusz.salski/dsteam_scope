/*
  ==============================================================================

    ChannelOGLVisualisation.cpp
    Created: 23 Sep 2019 9:13:25am
    Author:  Mateusz Salski

  ==============================================================================
*/

#include "ChannelWaveformShape.h"

namespace dsteam
{
    void ChannelWaveformShape::init(OpenGLContext& openGLContext)
    {
        openGLContext.extensions.glGenBuffers (1, &minBufferID);
        openGLContext.extensions.glGenBuffers (1, &maxBufferID);
    }
    
    void ChannelWaveformShape::updateVerts(std::vector<Range<float>>& scopeData, float chZero, float chHeight)
    {
        ScopedWriteLock writeLock(vertsLock);
        
        numVerts = (int) scopeData.size();
        float minx = -1.f;
        float maxx = 1.f;
        float xd = (maxx - minx) / (float) numVerts;
        
        /*
         BUG: numVerts and maxNumVerts are not correlated, so it is possible that 2 * numVerts will be greater than maxNumVerts.
         In this case not all vertex will be drawn.
         */
        
        for (int i = 0, j = 0; i < numVerts && j < maxNumVerts; j += 2, ++i)
        {
            minVerts[j] = minx + (float) i * xd;
            minVerts[j + 1] = GLfloat(scopeData[i].getStart());
            
            maxVerts[j] = minx + (float) i * xd;
            maxVerts[j + 1] = GLfloat(scopeData[i].getEnd());
        }
        
        Matrix3D<float> scale { AffineTransform::scale(1.f, chHeight)};
        transform = scale * Matrix3D<float> { Vector3D<float> { .0f, chZero, .0f } };
        
        ready = true;
    }
    
    void ChannelWaveformShape::render(OpenGLContext& openGLContext, GLint posAttrib, GLuint shaderProgram)
    {
        if (!ready)
            return;
        
        const ScopedReadLock readLock(vertsLock);
        
        
        openGLContext.extensions.glBindBuffer (GL_ARRAY_BUFFER, minBufferID);
        
        openGLContext.extensions.glBufferData (GL_ARRAY_BUFFER, sizeof (minVerts), minVerts, GL_STREAM_DRAW);
        
        openGLContext.extensions.glVertexAttribPointer (posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
        openGLContext.extensions.glEnableVertexAttribArray (posAttrib);
        
        GLint transformation = glGetUniformLocation(shaderProgram, "transformation");
        glUseProgram(shaderProgram);
        
        glUniformMatrix4fv(transformation, 1, GL_FALSE, transform.mat);
        
        glDrawArrays (GL_LINE_STRIP, 0, numVerts);
        
        
        openGLContext.extensions.glBindBuffer (GL_ARRAY_BUFFER, maxBufferID);
        
        openGLContext.extensions.glBufferData (GL_ARRAY_BUFFER, sizeof (maxVerts), maxVerts, GL_STREAM_DRAW);
        
        openGLContext.extensions.glVertexAttribPointer (posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
        openGLContext.extensions.glEnableVertexAttribArray (posAttrib);
        glUseProgram(shaderProgram);
        glDrawArrays (GL_LINE_STRIP, 0, numVerts);
    }
}
