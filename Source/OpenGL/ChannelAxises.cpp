/*
  ==============================================================================

    ChannelAxises.cpp
    Created: 2 Oct 2019 9:34:58am
    Author:  Mateusz Salski

  ==============================================================================
*/

#include "ChannelAxises.h"

namespace dsteam
{
    void ChannelAxises::init(OpenGLContext& openGLContext)
    {
        openGLContext.extensions.glGenBuffers (1, &horizontalLineBufferId);
    }

    void ChannelAxises::render(OpenGLContext& openGLContext, GLint posAttrib, GLuint shaderProgram) {
        openGLContext.extensions.glBindBuffer (GL_ARRAY_BUFFER, horizontalLineBufferId);
        
        openGLContext.extensions.glBufferData (GL_ARRAY_BUFFER, sizeof (horizontalLineBuffer), horizontalLineBuffer, GL_STREAM_DRAW);
        
        openGLContext.extensions.glVertexAttribPointer (posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
        openGLContext.extensions.glEnableVertexAttribArray (posAttrib);
        glUseProgram(shaderProgram);
        glDrawArrays (GL_LINE_STRIP, 0, 2);
    }
}
