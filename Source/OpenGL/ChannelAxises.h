/*
  ==============================================================================

    ChannelAxises.h
    Created: 2 Oct 2019 9:34:58am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace dsteam
{
    
    class ChannelAxises
    {
    public:
        void init(OpenGLContext& openGLContext);
        
        void render(OpenGLContext&, GLint, GLuint);
        
    private:
        GLuint horizontalLineBufferId;
        GLfloat horizontalLineBuffer[4] { -1.f, 0.f, 1.f, 0.f };
    };
}
