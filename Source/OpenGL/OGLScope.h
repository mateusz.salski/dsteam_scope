/*
  ==============================================================================

    OpenGLScopePainter.h
    Created: 23 Sep 2019 12:45:10pm
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ChannelWaveformShape.h"
#include "ChannelAxises.h"

namespace dsteam
{
    class OGLScope : public OpenGLAppComponent
    {
    public:
        
        virtual ~OGLScope()
        {
            openGLContext.detach();
        }
        
        void paint(Graphics &g) override
        {
        }
        
        void updateVerts(std::vector<std::vector<Range<float>>>& scopeData);
        
    private:
        void initialise() override;
        
        void render() override;
        
        void shutdown() override {}
        
        std::vector<ChannelWaveformShape> channelWaveformShapes { 2 };
        ChannelAxises chAxises;
        
        GLint posAttrib;
        GLuint shaderProgram;
        
        const char* vertexSource = R"glsl(
        attribute vec2 position;
        
        uniform mat4 transformation;
        
        varying vec4 vertexColour;
        
        void main()
        {
            gl_Position = transformation * vec4(position, 0.0, 1.0);
            vertexColour = vec4(1.0 - position.y, position.y, 0.0, 1.0);
        }
        )glsl";
        
        const char* fragmentSource = R"glsl(
        varying vec4 vertexColour;
        void main()
        {
            gl_FragColor = vertexColour;
        }
        )glsl";
    };
}
