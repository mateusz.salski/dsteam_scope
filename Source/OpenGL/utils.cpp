/*
  ==============================================================================

    utils.cpp
    Created: 24 Sep 2019 10:41:51am
    Author:  Mateusz Salski

  ==============================================================================
*/

#include "utils.h"

namespace dsteam
{
    
    GLuint  createShader(const char* source, GLenum type)
    {
        GLuint shaderId = glCreateShader(type);
        glShaderSource(shaderId, 1, &source, NULL);
        glCompileShader(shaderId);
        
#ifdef DEBUG
        GLint status;
        glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);
        DBG("Status shader: " << status);
        
        char buffer[512];
        glGetShaderInfoLog(shaderId, 512, NULL, buffer);
        DBG("Log shader: " << buffer);
#endif
        
        return shaderId;
    }
    
}
