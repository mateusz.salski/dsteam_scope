/*
  ==============================================================================

    ChannelOGLVisualisation.h
    Created: 23 Sep 2019 9:13:25am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace dsteam
{

    class ChannelWaveformShape
    {
    public:
        ChannelWaveformShape() = default;
        ChannelWaveformShape(const ChannelWaveformShape& other)
        : vertsLock()
        {
            minBufferID = other.minBufferID;
            maxBufferID = other.maxBufferID;
            
            numVerts = other.numVerts;
            
            ready = false;
        }
        
        ChannelWaveformShape(ChannelWaveformShape&& old)
        : vertsLock()
        {
            minBufferID = old.minBufferID;
            maxBufferID = old.maxBufferID;
            
            numVerts = old.numVerts;

            ready = false;
        }
        
        void init(OpenGLContext&);
        
        // Updates vertex data in OGL buffers
        void updateVerts(std::vector<Range<float>>& scopeData, float chZero, float chHeight);
        
        // Renders vertex buffers
        void render(OpenGLContext&, GLint, GLuint);
        
    private:
        GLuint minBufferID;
        GLuint maxBufferID;
        
        const static int maxNumVerts = 2400;
        GLfloat minVerts[maxNumVerts];
        GLfloat maxVerts[maxNumVerts];
        int numVerts { maxNumVerts };
        ReadWriteLock vertsLock;
        
        Matrix3D<float> transform;
        
        bool ready { false };
    };

}
