/*
  ==============================================================================

    OpenGLScopePainter.cpp
    Created: 23 Sep 2019 12:45:10pm
    Author:  Mateusz Salski

  ==============================================================================
*/

#include "OGLScope.h"
#include "utils.h"

namespace dsteam
{
    void OGLScope::initialise()
    {
        char* v = (char*) glGetString(GL_VERSION);
        DBG("OpenGL version: " << v);
        
        GLuint vertexShader = dsteam::createShader(vertexSource, GL_VERTEX_SHADER);
        GLuint fragmentShader = dsteam::createShader(fragmentSource, GL_FRAGMENT_SHADER);
        
        shaderProgram = glCreateProgram();
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, fragmentShader);
        
        glBindFragDataLocation(shaderProgram, 0, "outColor");
        
        glLinkProgram(shaderProgram);
        glUseProgram(shaderProgram);
        
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        
        posAttrib = glGetAttribLocation(shaderProgram, "position");
        
        for (int c = 0; c < channelWaveformShapes.size(); ++c)
            channelWaveformShapes[c].init(openGLContext);
        
        chAxises.init(openGLContext);
    }


    void OGLScope::updateVerts(std::vector<std::vector<Range<float>>>& scopeData)
    {
        int oldSize = static_cast<int>(channelWaveformShapes.size());
        int newSize = static_cast<int>(scopeData.size());
        
        if (newSize != oldSize)
            channelWaveformShapes.resize(scopeData.size());
        
        for (int i = oldSize; i < newSize; ++i)
            channelWaveformShapes[i].init(openGLContext);
        
        for (int channelIdx = 0; channelIdx < channelWaveformShapes.size(); ++channelIdx)
        {
            float chHeight = 1.f / (float) static_cast<int>(channelWaveformShapes.size());
            float chZero = 1.f - chHeight - channelIdx * 2.f * chHeight;
            
            channelWaveformShapes[channelIdx].updateVerts(scopeData[channelIdx], chZero, chHeight);
        }
    }

    void OGLScope::render() {
        OpenGLHelpers::clear(Colours::black);
        
        for (int c = 0; c < channelWaveformShapes.size(); ++c)
        {
            channelWaveformShapes[c].render(openGLContext, posAttrib, shaderProgram);
            chAxises.render(openGLContext, posAttrib, shaderProgram);
        }
    }
}
