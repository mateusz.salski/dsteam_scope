/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Dsteam_scopeAudioProcessorEditor::Dsteam_scopeAudioProcessorEditor (Dsteam_scopeAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setResizable(true, true);
    setSize (1200, 350);
    
    zoomSlider.setSliderStyle(Slider::SliderStyle::TwoValueHorizontal);
    zoomSlider.setRange(0, 1, 1);
    zoomSlider.setMinAndMaxValues(0, 1);
    zoomSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);
    
   // channelsSeparation.setSliderStyle(Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    channelsSeparation.setRange(0.0, 1.0);
    channelsSeparation.setValue(1.0f);
    
    addAndMakeVisible(scope);
    addAndMakeVisible(freezButton);
    addAndMakeVisible(zoomSlider);
    addAndMakeVisible(channelsSeparation);
    
    freezButton.onClick = [this]()
    {
        scope.freez();
    };
    
    zoomSlider.addListener(this);
    channelsSeparation.addListener(this);
}

Dsteam_scopeAudioProcessorEditor::~Dsteam_scopeAudioProcessorEditor()
{
}

void Dsteam_scopeAudioProcessorEditor::prepare(double sampleRate)
{
    scope.prepare(sampleRate);
    
    zoomSlider.setRange(0, scope.getNumSamples(), 1);
    zoomSlider.setMinAndMaxValues(0, scope.getNumSamples());
}

void Dsteam_scopeAudioProcessorEditor::release()
{
    scope.release();
}

//==============================================================================
void Dsteam_scopeAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void Dsteam_scopeAudioProcessorEditor::resized()
{
    auto windowArea = getLocalBounds();
    scope.setBounds(windowArea.removeFromTop(getHeight() * 300.f / 350.f));
    
    auto bottomArea = windowArea.removeFromBottom(windowArea.getHeight() / 2);
    freezButton.setBounds(bottomArea.removeFromLeft(getWidth() * 100.f / 1200.f));
    channelsSeparation.setBounds(bottomArea);
    freezButton.changeWidthToFitText();
    
    zoomSlider.setBounds(windowArea);
}


void Dsteam_scopeAudioProcessorEditor::sliderValueChanged (Slider* slider)
{
    if (slider == &zoomSlider)
    {
        scope.setPosition(static_cast<int>(zoomSlider.getMinValue()));
        scope.setNumSamplesVisible(static_cast<int>(zoomSlider.getMaxValue() - zoomSlider.getMinValue()));
    }
    else if (slider == &channelsSeparation)
    {
        scope.setChannelSeparation(channelsSeparation.getValue());
    }
}
