/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "dsteam_Scope.h"

//==============================================================================
/**
*/
class Dsteam_scopeAudioProcessorEditor  : public AudioProcessorEditor, Slider::Listener
{
public:
    Dsteam_scopeAudioProcessorEditor (Dsteam_scopeAudioProcessor&);
    ~Dsteam_scopeAudioProcessorEditor() override;
    
    void prepare(double sampleRate);
    void release();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Dsteam_scopeAudioProcessor& processor;
    
    dsteam::Scope scope { processor.getAudioBufferQueue() };
    
    ToggleButton freezButton { "freez" };
    Slider numSamplesVisibleSlider { "Num visible samples" };
    Slider positionSlider { "Position" };
    
    Slider zoomSlider { "zoom" };
    Slider channelsSeparation { "channels separation" };
    
    void sliderValueChanged (Slider* slider) override;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Dsteam_scopeAudioProcessorEditor)
};
