/*
  ==============================================================================

    dsteam_Scope.cpp
    Created: 3 Sep 2019 6:56:36am
    Author:  Mateusz Salski

  ==============================================================================
*/

#include "dsteam_Scope.h"
#include <fstream>
#include <sstream>

namespace dsteam
{
    std::string readFileAsString(const char* filePath)
    {
        std::ifstream inputStream;
        inputStream.exceptions (std::ifstream::failbit | std::ifstream::badbit);
        
        try
        {
            inputStream.open(filePath);
            std::stringstream ss;
            ss << inputStream.rdbuf();
            inputStream.close();
            return ss.str();
        }
        catch (std::ifstream::failure e)
        {
            std::cout << "ERROR::FILE_NOT_SUCCESFULLY_READ: " << filePath << std::endl;
        }
        
        return "";
    }
    
    Colour getChannelColour(int ch)
    {
        switch (ch)
        {
            case 0: return Colours::green;
            case 1: return Colours::red;
            default: return Colours::grey;
        }
    }
    
    Scope::Scope(dsteam::AudioBufferQueue<float>& audioBufferQueue)
    : audioBufferQueue(audioBufferQueue)
    {
        openGLContext.setOpenGLVersionRequired(juce::OpenGLContext::openGL3_2);
    }
    
    Scope::~Scope()
    {
        shutdownOpenGL();
    }
    
    void Scope::prepare(double sampleRate)
    {
        ScopedWriteLock swl { memoryLock };
        
        stopTimer();
        memory.setSize(audioBufferQueue.getNumChannels(), 4 * sampleRate);
        
        channelShapes.resize(audioBufferQueue.getNumChannels());
        
        startTimer(60);
    }
    
    void Scope::release()
    {
        stopTimer();
        
        for (auto channelShape : channelShapes)
            channelShape.release();
        
        channelShapes.clear();
    }
    
    void Scope::freez()
    {
        if (isTimerRunning())
            stopTimer();
        else
            startTimer(60);
    }
    
    void Scope::setNumSamplesVisible(int newNumSamplesVisible)
    {
        //jassert(newNumSamplesVisible < memory.buffer.getNumSamples() - position);
        
        numSamplesVisible = newNumSamplesVisible;
    }
    
    void Scope::setPosition(int newPosition)
    {
        //jassert(numSamplesVisible < memory.buffer.getNumSamples() - newPosition);
        
        position = newPosition;
    }
    
    void Scope::timerCallback()
    {
        ScopedWriteLock swl { memoryLock };
        
        int numSamplesToRead = audioBufferQueue.getNumReady();
        AudioBuffer<float> tmpBuffer { audioBufferQueue.getNumChannels(), numSamplesToRead };
        audioBufferQueue.pop(tmpBuffer, numSamplesToRead);
        memory.copyFrom(tmpBuffer);
        
        
    }

    // ----- OpenGL staff ----------------------------------
    
    void Scope::initialise()
    {
        oglProgram.addVertexShader(String (BinaryData::scope_vs)); //readFileAsString("../../../../Shaders/scope.vs"));//String (BinaryData::scope_vs));
        oglProgram.addFragmentShader(String (BinaryData::scope_fs)); //readFileAsString("../../../../Shaders/scope.fs"));//String (BinaryData::scope_fs));
        oglProgram.link();
        
        for (int ch = 0; ch < channelShapes.size(); ++ch)
            channelShapes[ch].initialise(memory.buffer.getReadPointer(ch), memory.buffer.getNumSamples(), getChannelColour(ch));
    }
    
    Matrix3D<float> Scope::getTransformationMatrix(int ch)
    {
        float chHeight = 1.f / (float) memory.buffer.getNumChannels();
        float chZero = 1.f - chHeight - ch * 2.f * chHeight;
        
        Matrix3D<float> scale { AffineTransform::scale(1.f, chHeight)};
        Matrix3D<float> transform = scale * Matrix3D<float> { Vector3D<float> { .0f, channelSeparation * chZero, .0f } };
        
        return transform;
    }
    
    void Scope::render()
    {
        ScopedReadLock srl { memoryLock };
        
        OpenGLHelpers::clear (Colours::black);
        
        oglProgram.use();
        
        for (int ch = 0; ch < channelShapes.size(); ++ch)
        {
            oglProgram.setUniformMat4("transformation", getTransformationMatrix(ch).mat, 1, GL_FALSE);
            channelShapes[ch].showDots = numSamplesVisible < getWidth() / 4;
            channelShapes[ch].render(oglProgram, memory, ch, position, numSamplesVisible);
        }
    }
    
    void Scope::shutdown()
    {
    }
}
