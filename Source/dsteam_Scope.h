/*
  ==============================================================================

    dsteam_Scope.h
    Created: 3 Sep 2019 6:56:36am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#include <vector>

#include "dsteam_AudioBufferQueue.h"
#include "dsteam_ChannelWaveformShape.h"

namespace dsteam
{
    class Scope : public OpenGLAppComponent, private Timer
    {
    public:
        
        Scope(AudioBufferQueue<float>& audioBufferQueue);
        virtual ~Scope();
        
        void paint(Graphics& g) override {} ;
        
        void prepare(double sampleRate);
        void release();
        
        void freez();
        
        void setNumSamplesVisible(int newNumSamplesVisible);
        void setPosition(int newPosition);
        void setChannelSeparation(float chSep) { channelSeparation = chSep; }
        
        int getNumSamples() { return memory.buffer.getNumSamples(); }
        
    private:
        int numSamplesVisible { 0 };
        int position { 0 };
        float channelSeparation { 1.0 };
        
        // Source of audio data
        dsteam::AudioBufferQueue<float>& audioBufferQueue;
        
        dsteam::CircularBuffer<float> memory;
        ReadWriteLock memoryLock;
        
        // ---- OpenGL staff -------------------------------------------------------------------
        OpenGLShaderProgram oglProgram { openGLContext };
        
        std::vector<ChannelWaveformShape> channelShapes;
        
        void initialise() override;
        void render() override;
        void shutdown() override;
        
        Matrix3D<float> getTransformationMatrix(int ch);
        
        // Updates memory
        void timerCallback() override;
    };
}
