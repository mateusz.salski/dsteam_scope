/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Dsteam_scopeAudioProcessor::Dsteam_scopeAudioProcessor() :
#ifndef JucePlugin_PreferredChannelConfigurations
     AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       ),
#endif
    audioBufferQueue(2, audioBufferQueueSize)
{
}

Dsteam_scopeAudioProcessor::~Dsteam_scopeAudioProcessor()
{
}

//==============================================================================
const String Dsteam_scopeAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Dsteam_scopeAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Dsteam_scopeAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool Dsteam_scopeAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double Dsteam_scopeAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Dsteam_scopeAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Dsteam_scopeAudioProcessor::getCurrentProgram()
{
    return 0;
}

void Dsteam_scopeAudioProcessor::setCurrentProgram (int index)
{
}

const String Dsteam_scopeAudioProcessor::getProgramName (int index)
{
    return {};
}

void Dsteam_scopeAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Dsteam_scopeAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    audioBufferQueue.resize(getTotalNumInputChannels(), audioBufferQueueSize);
    auto* editor = dynamic_cast<Dsteam_scopeAudioProcessorEditor*>(getActiveEditor());
    if (editor != nullptr)
        editor->prepare(sampleRate);
}

void Dsteam_scopeAudioProcessor::releaseResources()
{
    auto* editor = dynamic_cast<Dsteam_scopeAudioProcessorEditor*>(getActiveEditor());
    if (editor != nullptr)
        editor->release();
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Dsteam_scopeAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
    return true;
}
#endif

void Dsteam_scopeAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    
    if (getTotalNumInputChannels() < getTotalNumOutputChannels())
    {
        AudioBuffer<float> subBuffer { buffer.getArrayOfWritePointers(), getTotalNumInputChannels(), buffer.getNumSamples() };
        audioBufferQueue.put(subBuffer);
    }
    else
    {
        audioBufferQueue.put(buffer);
    }
    
}

//==============================================================================
bool Dsteam_scopeAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Dsteam_scopeAudioProcessor::createEditor()
{
    auto* editor = new Dsteam_scopeAudioProcessorEditor (*this);
    if (editor != nullptr)
        editor->prepare(getSampleRate());
    
    return editor;
}

//==============================================================================
void Dsteam_scopeAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void Dsteam_scopeAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Dsteam_scopeAudioProcessor();
}
