/*
  ==============================================================================

    ChannelOGLVisualisation.h
    Created: 23 Sep 2019 9:13:25am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace dsteam
{
    struct ChannelWaveformShape
    {
        GLuint VAO;
        GLuint VBO;
        GLuint posAttId;
        
        bool showDots { false };
        Colour colour;
        
        void initialise(const float* data, int numSamples, Colour c);
        void render(OpenGLShaderProgram& oglProgram, CircularBuffer<float>, int channelIndex, int viewBegin, int viewEnd);
        void release();
    };
}
