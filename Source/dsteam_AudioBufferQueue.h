/*
  ==============================================================================

    AudioBufferQueue.h
    Created: 27 Sep 2019 11:34:10am
    Author:  Mateusz Salski

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace dsteam
{
    
    template <typename SampleType>
    void copy(const AudioBuffer<SampleType>& src, int srcStart, AudioBuffer<SampleType>& dst, int dstStart, int numSamples)
    {
        jassert(src.getNumChannels() <= dst.getNumChannels());
        jassert(numSamples <= dst.getNumSamples());
        
        for (int channelIdx = 0; channelIdx < src.getNumChannels(); ++channelIdx)
            dst.copyFrom(channelIdx, dstStart, src, channelIdx, srcStart, numSamples);
    }
    
    template <typename SampleType>
    struct AudioBufferQueue
    {
        AudioBufferQueue(int numChannels, int numSamples)
        : audioBuffer(numChannels, numSamples), abstractFifo(numSamples)
        {}
        
        void resize(int numChannels, int numSamples)
        {
            audioBuffer.setSize(numChannels, numSamples);
            abstractFifo.setTotalSize(numSamples);
        }
        
        /*
         *  Puts as much of data in src as it is possible to the queue. Always starting from begining of src.
         */
        void put (const AudioBuffer<SampleType>& src)
        {
            jassert(audioBuffer.getNumSamples() >= src.getNumSamples());
            
            int start1, size1, start2, size2;
            abstractFifo.prepareToWrite (src.getNumSamples(), start1, size1, start2, size2);
            
            if (size1 > 0)
                dsteam::copy(src, 0, audioBuffer, start1, size1);
            
            if (size2 > 0)
                dsteam::copy(src, size1, audioBuffer, start2, size2);
            
            abstractFifo.finishedWrite (size1 + size2);
        }
        
        /*
         *  Pops data from queue.
         */
        void pop (AudioBuffer<SampleType>& dst, int& numSamples) // co z dstStart? co jesli dstSize + sizeX < size1 lub siez3?
        {
            int start1, size1, start2, size2;
            abstractFifo.prepareToRead (dst.getNumSamples(), start1, size1, start2, size2);
            
            if (size1 > 0)
                dsteam::copy(audioBuffer, start1, dst, 0, size1);
            
            if (size2 > 0)
                dsteam::copy(audioBuffer, start2, dst, 0 + size1, size2);
            
            numSamples = size1 + size2;
            abstractFifo.finishedRead (size1 + size2);
        }
        
        int getNumReady() { return abstractFifo.getNumReady(); }
        
        int getNumSamples() { return audioBuffer.getNumSamples(); }
        
        int getNumChannels() { return audioBuffer.getNumChannels(); }
        
    private:
        AudioBuffer<SampleType> audioBuffer;
        AbstractFifo abstractFifo;
    };
}

