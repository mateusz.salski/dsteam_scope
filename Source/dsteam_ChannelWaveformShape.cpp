/*
  ==============================================================================

    ChannelOGLVisualisation.cpp
    Created: 23 Sep 2019 9:13:25am
    Author:  Mateusz Salski

  ==============================================================================
*/

#include "dsteam_ChannelWaveformShape.h"

namespace dsteam
{
    void ChannelWaveformShape::initialise(const float* data, int numSamples, Colour c)
    {
        colour = c;
        
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        
        glBindVertexArray(VAO);
        {
            glBindBuffer(GL_ARRAY_BUFFER, VBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(float) * numSamples, data, GL_STREAM_DRAW);
            
            glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        
        glBindVertexArray(0);
    }
    
    void ChannelWaveformShape::release()
    {
        glDeleteVertexArrays(1, &VAO);
        glDeleteBuffers(1, &VBO);
    }
    
    void ChannelWaveformShape::render(OpenGLShaderProgram& oglProgram, CircularBuffer<float> memory, int channelIndex, int viewBegin, int viewLen)
    {
        auto* data = memory.buffer.getReadPointer(channelIndex);
        
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        
        auto len = memory.buffer.getNumSamples();
        auto pos = memory.getCurrentPosition();
        auto viewEnd = viewBegin + viewLen;
        auto b1 = (pos + viewBegin) % len;
        auto b2 = (pos + viewEnd - 1) % len;
        
        oglProgram.setUniform("dx", 2.f / viewLen);
        oglProgram.setUniform("chColor", colour.getFloatRed(), colour.getFloatGreen(), colour.getFloatBlue(), colour.getFloatAlpha());
        
        if (b1 < b2)
        {
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * viewLen, data + b1);
        }
        else if (b1 > b2)
        {
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * (len - b1), data + b1);
            glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * (len - b1), sizeof(float) * (viewLen + b1 - len), data);
        }
        else
            jassertfalse;
    
        glDrawArrays(GL_LINE_STRIP, 0, viewLen);
        
        if (showDots)
        {
            glPointSize(4);
            glDrawArrays(GL_POINTS, 0, viewLen);
        }
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }
}
