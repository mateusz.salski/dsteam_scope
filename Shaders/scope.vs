#version 330 core

layout (location = 0) in float position;

uniform float dx;
uniform mat4 transformation;
uniform vec4 chColor;

out vec4 vColor;

void main()
{
    gl_Position = transformation * vec4(-1.0 + gl_VertexID * dx, position, 0.0, 1.0f);
    vColor = chColor;//vec4(gl_VertexID * dx / 2, 1.0 - gl_VertexID * dx / 2, -1.0 + gl_VertexID * dx, 1.0);
}
