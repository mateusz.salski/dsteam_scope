/* ==================================== JUCER_BINARY_RESOURCE ====================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

namespace BinaryData
{

//================== scope.fs ==================
static const unsigned char temp_binary_data_0[] =
"#version 330 core\n"
"in vec4 vColor;\n"
"out vec4 color;\n"
"\n"
"void main()\n"
"{\n"
"    color = vColor;\n"
"}\n";

const char* scope_fs = (const char*) temp_binary_data_0;

//================== scope.vs ==================
static const unsigned char temp_binary_data_1[] =
"#version 330 core\n"
"\n"
"layout (location = 0) in float position;\n"
"\n"
"uniform float dx;\n"
"uniform mat4 transformation;\n"
"uniform vec4 chColor;\n"
"\n"
"out vec4 vColor;\n"
"\n"
"void main()\n"
"{\n"
"    gl_Position = transformation * vec4(-1.0 + gl_VertexID * dx, position, 0.0, 1.0f);\n"
"    vColor = chColor;//vec4(gl_VertexID * dx / 2, 1.0 - gl_VertexID * dx / 2, -1.0 + gl_VertexID * dx, 1.0);\n"
"}\n";

const char* scope_vs = (const char*) temp_binary_data_1;


const char* getNamedResource (const char* resourceNameUTF8, int& numBytes)
{
    unsigned int hash = 0;

    if (resourceNameUTF8 != nullptr)
        while (*resourceNameUTF8 != 0)
            hash = 31 * hash + (unsigned int) *resourceNameUTF8++;

    switch (hash)
    {
        case 0xe2f3a298:  numBytes = 87; return scope_fs;
        case 0xe2f3a488:  numBytes = 361; return scope_vs;
        default: break;
    }

    numBytes = 0;
    return nullptr;
}

const char* namedResourceList[] =
{
    "scope_fs",
    "scope_vs"
};

const char* originalFilenames[] =
{
    "scope.fs",
    "scope.vs"
};

const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8)
{
    for (unsigned int i = 0; i < (sizeof (namedResourceList) / sizeof (namedResourceList[0])); ++i)
    {
        if (namedResourceList[i] == resourceNameUTF8)
            return originalFilenames[i];
    }

    return nullptr;
}

}
